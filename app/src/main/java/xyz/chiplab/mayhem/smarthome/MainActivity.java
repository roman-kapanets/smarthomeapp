package xyz.chiplab.mayhem.smarthome;

import android.content.DialogInterface;
import android.net.wifi.WifiConfiguration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.content.IntentFilter;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends AppCompatActivity {
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    ListView ListMain;

    StringBuilder sb = new StringBuilder();

    private final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListMain = (ListView) findViewById(R.id.lv1);
        mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        receiverWifi = new WifiReceiver();
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        if (!mainWifi.isWifiEnabled()) {
            mainWifi.setWifiEnabled(true);
        }


        doInback();

        ListMain.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Object object = ListMain.getItemAtPosition(position);
                DataModel dataModel = (DataModel) object;

                String Capabilities = dataModel.getEncrypt();
                final String networkSSID = dataModel.getSSID();
                int encryptType = 0;
                if (Capabilities.contains("WEP")) {
                    encryptType = 1;
                } else if (Capabilities.contains("WPA")) {
                    encryptType = 2;
                }
                final int finalEncryptType = encryptType;
                if (finalEncryptType > 0) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.setTitle(getString(R.string.connnect_to)+": "+networkSSID);
                    alert.setMessage(R.string.wifi_pass_enter);

                    final EditText input = new EditText(MainActivity.this);
                    input.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    alert.setView(input);

                    alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            String pass = input.getText().toString();
                            Wifi connection = new Wifi();
                            if (connection.Connect(networkSSID, finalEncryptType, pass)){
                                Intent intent = new Intent(MainActivity.this, ControlActivity.class);
                                // add the selected text item to our intent.
                                intent.putExtra("networkSSID", networkSSID);
                                startActivity(intent);
                            }
                        }
                    });

                    alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {

                        }
                    });

                    alert.show();
                } else {
                    Wifi connection = new Wifi();
                    if (connection.Connect(networkSSID, finalEncryptType, "")){
                        Intent intent = new Intent(MainActivity.this, ControlActivity.class);
                        // add the selected text item to our intent.
                        intent.putExtra("networkSSID", networkSSID);
                        startActivity(intent);
                    }
                }
            }
        });
    }


    public void doInback() {
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new WifiReceiver();
                registerReceiver(receiverWifi, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                mainWifi.startScan();
                doInback();
            }
        }, 1500);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.refresh);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        mainWifi.startScan();

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        unregisterReceiver(receiverWifi);
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

     class WifiReceiver extends BroadcastReceiver {
        ArrayList<DataModel> dataModels;

        public void onReceive(Context c, Intent intent) {
            sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();

            int size = wifiList.size();
            dataModels = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                String Capabilities = wifiList.get(i).capabilities;
                String encryptType = "";
                if (Capabilities.contains("WPA2")) {
                    encryptType = "WPA2";
                } else if (Capabilities.contains("WPA")) {
                    encryptType = "WPA";
                } else if (Capabilities.contains("WEP")) {
                    encryptType = "WEP";
                }

                dataModels.add(new DataModel(wifiList.get(i).SSID, encryptType, wifiList.get(i).level));

            }
            ListMain.setAdapter(new WifiArrayAdapter(getApplicationContext(), dataModels));
        }
    }

    class Wifi {

        private boolean Connect(String networkSSID, int encrypt, String password) {
            // encrypt:
            // 0 - None
            // 1 - WEP
            // 2 - WPA
            boolean result;
            result = false;
            switch (encrypt) {
                case 1:
                    result = ConnectToNetworkWEP(networkSSID, password);
                    break;
                case 2:
                    result = ConnectToNetworkWPA(networkSSID, password);
                    break;
                default:
                    result = ConnectToNetworkOpen(networkSSID);
                    break;
            }
            return result;
        }

        private boolean ConnectToNetworkWEP(String networkSSID, String password) {
            try {
                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain SSID in quotes
                conf.wepKeys[0] = "\"" + password + "\""; //Try it with quotes first

                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedGroupCiphers.set(WifiConfiguration.AuthAlgorithm.OPEN);
                conf.allowedGroupCiphers.set(WifiConfiguration.AuthAlgorithm.SHARED);


                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                int networkId = wifiManager.addNetwork(conf);

                if (networkId == -1) {
                    //Try it again with no quotes in case of hex password
                    conf.wepKeys[0] = password;
                    networkId = wifiManager.addNetwork(conf);
                }

                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                for (WifiConfiguration i : list) {
                    if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                        wifiManager.disconnect();
                        wifiManager.enableNetwork(i.networkId, true);
                        wifiManager.reconnect();
                        break;
                    }
                }

                //WiFi Connection success, return true
                return true;
            } catch (Exception ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
                return false;
            }
        }

        private boolean ConnectToNetworkWPA(String networkSSID, String password) {
            try {
                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain SSID in quotes

                conf.preSharedKey = "\"" + password + "\"";

                conf.status = WifiConfiguration.Status.ENABLED;
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

                Log.d("connecting", conf.SSID + " " + conf.preSharedKey);

                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiManager.addNetwork(conf);

                Log.d("after connecting", conf.SSID + " " + conf.preSharedKey);


                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                for (WifiConfiguration i : list) {
                    if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                        wifiManager.disconnect();
                        wifiManager.enableNetwork(i.networkId, true);
                        wifiManager.reconnect();
                        Log.d("re connecting", i.SSID + " " + conf.preSharedKey);

                        break;
                    }
                }


                //WiFi Connection success, return true
                return true;
            } catch (Exception ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
                return false;
            }
        }

        private boolean ConnectToNetworkOpen(String networkSSID) {
            try {
                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain SSID in quotes
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                Log.d("connecting", conf.SSID + " " + conf.preSharedKey);
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiManager.addNetwork(conf);
                Log.d("after connecting", conf.SSID + " " + conf.preSharedKey);

                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                for (WifiConfiguration i : list) {
                    if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                        wifiManager.disconnect();
                        wifiManager.enableNetwork(i.networkId, true);
                        wifiManager.reconnect();
                        Log.d("re connecting", i.SSID + " " + conf.preSharedKey);

                        break;
                    }
                }

                return true;
            } catch (Exception ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
                return false;
            }

        }
    }
    /*public void connectToNetwork(String ssid){

        WifiInfo info = mWifiManager.getConnectionInfo(); //get WifiInfo
        int id = info.getNetworkId(); //get id of currently connected network

        for (WifiConfiguration config : configurations) {
            // If it was cached connect to it and that's all
            if (config.SSID != null && config.SSID.equals("\"" +ssid + "\"")) {
                // Log
                Log.i("connectToNetwork", "Connecting to: " + config.SSID);

                mWifiManager.disconnect();

                mWifiManager.disableNetwork(id); //disable current network

                mWifiManager.enableNetwork(config.networkId, true);
                mWifiManager.reconnect();
                break;
            }
        }
    }*/
}


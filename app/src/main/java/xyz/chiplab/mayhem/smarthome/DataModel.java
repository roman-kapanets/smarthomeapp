package xyz.chiplab.mayhem.smarthome;

public class DataModel {

    String SSID;
    String encrypt;
    int level;

    public DataModel(String _SSID, String _encrypt, int _level ) {
        this.SSID=_SSID;
        this.encrypt=_encrypt;
        this.level=_level;
    }

    public String getSSID() {
        return SSID;
    }

    public String getEncrypt() {
        return encrypt;
    }

    public int getLevel() {
        return level;
    }

}

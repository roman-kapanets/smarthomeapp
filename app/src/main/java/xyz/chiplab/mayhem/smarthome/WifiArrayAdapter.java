package xyz.chiplab.mayhem.smarthome;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WifiArrayAdapter extends ArrayAdapter<DataModel> {
    private final Context context;
    private final ArrayList<DataModel> dataSet;

    private static class ViewHolder {
        TextView label;
        TextView enc;
        ImageView icon;
    }

    public WifiArrayAdapter(Context context, ArrayList<DataModel> data) {
        super(context, R.layout.wifi_list_view, data);
        //super(context, R.layout.wifi_list_view, data);
        this.context = context;
        this.dataSet = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataModel dataModel = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.wifi_list_view, parent, false);
            viewHolder.label = (TextView) convertView.findViewById(R.id.label);
            viewHolder.enc = (TextView) convertView.findViewById(R.id.enc);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.label.setText(dataModel.getSSID());
        viewHolder.enc.setText(dataModel.getEncrypt());
        viewHolder.icon.setTag(position);
        if (dataModel.getLevel() >= -49){
            viewHolder.icon.setImageResource(R.drawable.fine);
        } else if (dataModel.getLevel() >= -75){
            viewHolder.icon.setImageResource(R.drawable.medium);
        } else {
            viewHolder.icon.setImageResource(R.drawable.low);
        }
        // Return the completed view to render on screen
        return convertView;

/*
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.wifi_list_view, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        textView.setText(values[position]);
        TextView textView2 = (TextView) rowView.findViewById(R.id.enc);
        textView2.setText(encrypt[position]);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        // Change icon for any wifi level
        if (level[position] >= -49){
            imageView.setImageResource(R.drawable.fine);
        } else if (level[position] >= -75){
            imageView.setImageResource(R.drawable.medium);
        } else {
            imageView.setImageResource(R.drawable.low);
        }

        return rowView;*/
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

}
